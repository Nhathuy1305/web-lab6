<%@ page import="com.main.workspace.AccountBean" %>
<%@ page import="java.nio.charset.StandardCharsets" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    AccountBean accountBean = (AccountBean) request.getAttribute("accountBean");
    String name = "";
    if (accountBean == null) {
        // accountBean is not in the request scope, try to get data from cookies
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("username")) {
                    name = URLDecoder.decode(cookie.getValue(), StandardCharsets.UTF_8);
                }
            }
        }
    } else {
        name = accountBean.getName();
    }
%>
<html>
<head>
    <title>Company's Products</title>
    <link type="text/css" rel="stylesheet" href="styles.css">
    <script>
        function createTable() {
            const table = document.createElement("table");
            table.id = "cartItems";

            const headerRow = document.createElement("tr");
            const headers = ["PRODUCT", "MANUFACTURER", "MADE IN", "PRICE", "QUANTITY"];
            headers.forEach(header => {
                const th = document.createElement("th");
                th.innerText = header;
                headerRow.appendChild(th);
            });

            table.appendChild(headerRow);
            document.body.appendChild(table);

            const contextPath = "<%= request.getContextPath() %>";
            const checkoutForm = document.createElement("form");
            checkoutForm.id = "checkout-form";
            checkoutForm.action = contextPath + "/ShoppingServlet";
            checkoutForm.method = "post";

            // Create checkout button
            const checkoutButton = document.createElement("button");
            checkoutButton.id = "checkout-button";
            checkoutButton.type = "submit";
            checkoutButton.name = "checkout";
            checkoutButton.innerText = "Checkout";

            const divElement = document.createElement("div");
            divElement.className = "container";

            divElement.appendChild(checkoutButton);

            checkoutForm.appendChild(divElement);

            document.body.appendChild(checkoutForm);
        }

        let products = [];
        let quantities = [];

        function addToCart(event) {
            event.preventDefault(); // Prevent form from submitting and refreshing the page

            const product = document.getElementById("product").value;
            const quantity = document.getElementById("quantity").value;

            const price = parseFloat(product.split(",").pop().trim());

            const tableRow = document.createElement("tr");
            const productCell = document.createElement("td");
            const manufacturerCell = document.createElement("td");
            const madeInCell = document.createElement("td");
            const priceCell = document.createElement("td");
            const quantityCell = document.createElement("td");
            const deleteCell = document.createElement("td");

            productCell.innerText = product.split(",")[0].trim();
            manufacturerCell.innerText = product.split(",")[1].trim();
            madeInCell.innerText = product.split(",")[2].trim();
            priceCell.innerText = price.toString();
            quantityCell.innerText = quantity;

            const deleteButton = document.createElement("button");
            deleteButton.className = "delete-button";
            deleteButton.innerText = "Delete";
            deleteButton.addEventListener("click", function() {
                tableRow.remove(); // Remove table row on button click

                // Check if there are any rows left in the table
                const table = document.getElementById("cartItems");
                if (table.getElementsByTagName("tr").length === 1) {
                    // If there are no rows left, hide the table and the "Checkout" button
                    table.style.display = "none";
                    document.getElementById("checkout-button").style.display = "none";
                }

                removeFromAccountBean(product, quantity);
            });
            quantityCell.appendChild(deleteButton);
            deleteCell.appendChild(deleteButton);

            tableRow.appendChild(productCell);
            tableRow.appendChild(manufacturerCell);
            tableRow.appendChild(madeInCell);
            tableRow.appendChild(priceCell);
            tableRow.appendChild(quantityCell);
            tableRow.appendChild(deleteCell);

            const cartItems = document.getElementById("cartItems");
            if (!cartItems) {
                createTable();
            } else if (cartItems.style.display === "none") {
                cartItems.style.display = "table";
                document.getElementById("checkout-button").style.display = "block";
            }
            document.getElementById("cartItems").appendChild(tableRow);

            const checkoutForm = document.getElementById("checkout-form");

            // Create hidden input elements for the product and quantity
            const productInput = document.createElement("input");
            productInput.type = "hidden";
            productInput.name = "product";
            productInput.value = product;

            const quantityInput = document.createElement("input");
            quantityInput.type = "hidden";
            quantityInput.name = "quantity";
            quantityInput.value = quantity;

            // Append the input elements to the form
            checkoutForm.appendChild(productInput);
            checkoutForm.appendChild(quantityInput);
        }

        function removeFromAccountBean(product, quantity) {
            // Create an AJAX request
            const xhr = new XMLHttpRequest();
            xhr.open("POST", "ShoppingServlet", true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            const params = "product=" + encodeURIComponent(product) + "&quantity=" + encodeURIComponent(quantity) + "&delete=true";

            xhr.onload = function() {
                if (xhr.status >= 200 && xhr.status < 300) {
                    console.log("Product removed successfully.");
                } else {
                    console.error("Failed to remove product.");
                }
            };

            xhr.send(params);
        }

        document.addEventListener("DOMContentLoaded", function() {
            document.getElementById("add-to-cart").addEventListener("submit", addToCart);
        });
    </script>
</head>
<body>
    <h1>Hi <%= name %></h1>
    <h2>Please select our product and its quantity</h2>
    <hr>
    <form id="add-to-cart" method="post">
        <label for="product"><b>Product:</b></label>
        <select id="product" name="product">
            <option value="Product 1, Manufacturer 1, Country 1, 4.95">Product 1 | Manufacturer 1 | Country 1 | 4.95</option>
            <option value="Product 2, Manufacturer 2, Country 2, 6.95">Product 2 | Manufacturer 2 | Country 2 | 6.95</option>
            <option value="Product 3, Manufacturer 3, Country 3, 6.95">Product 3 | Manufacturer 3 | Country 3 | 6.95</option>
            <option value="Product 4, Manufacturer 4, Country 4, 3.95">Product 4 | Manufacturer 4 | Country 4 | 3.95</option>
            <option value="Product 5, Manufacturer 5, Country 5, 4.95">Product 5 | Manufacturer 5 | Country 5 | 4.95</option>
            <option value="Product 6, Manufacturer 6, Country 6, 2.95">Product 6 | Manufacturer 6 | Country 6 | 2.95</option>
            <option value="Product 7, Manufacturer 7, Country 7, 4.95">Product 7 | Manufacturer 7 | Country 7 | 4.95</option>
            <option value="Product 8, Manufacturer 8, Country 8, 2.95">Product 8 | Manufacturer 8 | Country 8 | 2.95</option>
            <option value="Product 9, Manufacturer 9, Country 9, 5.95">Product 9 | Manufacturer 9 | Country 9 | 5.95</option>
            <option value="Product 10, Manufacturer 10, Country 10, 3.95">Product 10 | Manufacturer 10 | Country 10 | 3.95</option>
        </select>
        <label for="quantity"><b>Quantity:</b></label>
        <input type="number" id="quantity" name="quantity">
        <div class="container">
            <button class="container" type="submit">Add to cart</button>
        </div>
    </form>
</body>
</html>
