<%@ page import="com.main.workspace.AccountBean" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.nio.charset.StandardCharsets" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    AccountBean accountBean = (AccountBean) request.getAttribute("accountBean");

    String name = "";
    String cardNumber = "";
    String address = "";

    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("username")) {
                name = URLDecoder.decode(cookie.getValue(), StandardCharsets.UTF_8);
            } else if (cookie.getName().equals("cardNumber")) {
                cardNumber = URLDecoder.decode(cookie.getValue(), StandardCharsets.UTF_8);
            } else if (cookie.getName().equals("address")) {
                address = URLDecoder.decode(cookie.getValue(), StandardCharsets.UTF_8);
            }
        }
    }

    String[] productsArray = accountBean.getProduct();
    String[] quantitiesArray = accountBean.getQuantity();

    DecimalFormat df = new DecimalFormat("#.00");
%>
<html>
<head>
    <title>Products Checkout</title>
    <link type="text/css" rel="stylesheet" href="styles.css">
</head>
<body>
<h2>Customer: <%= name %></h2>
<h2>VISA Card Number: <%= cardNumber %></h2>
<h2>Address: <%= address %></h2>
<h3>Products List</h3>
<hr>
<table id="cartItems">
    <tr>
        <th>PRODUCT</th>
        <th>MANUFACTURER</th>
        <th>MADE IN</th>
        <th>PRICE</th>
        <th>QUANTITY</th>
        <th>SUBTOTAL</th>
    </tr>
    <%
        double total = 0;
        for (int i = 0; i < productsArray.length; i++) {
            String[] product = productsArray[i].split(",");
            String productName = product[0].trim();
            String manufacturer = product[1].trim();
            String madeIn = product[2].trim();
            double price = Double.parseDouble(product[3].trim());
            int quantity = Integer.parseInt(quantitiesArray[i].trim());
            double subtotal = price * quantity;
            total += subtotal;
    %>
    <tr>
        <td><%= productName %></td>
        <td><%= manufacturer %></td>
        <td><%= madeIn %></td>
        <td><%= price %></td>
        <td><%= quantity %></td>
        <td><%= df.format(subtotal) %></td>
    </tr>
    <%
        }
    %>
    <tr>
        <td colspan="5" style="text-align: right"><b>Total:</b></td>
        <td>$<%= df.format(total) %></td>
    </tr>
</table>
<button onclick="window.location.href='products.jsp'">Shop some more!</button>
<button onclick="window.location.href='account.jsp'">Logout</button>
</body>
</html>
