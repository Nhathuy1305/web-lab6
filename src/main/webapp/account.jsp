<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Account Information</title>
    <link type="text/css" rel="stylesheet" href="styles.css">
</head>
<body>
<h1>Please provide your account information</h1>
<form action="ShoppingServlet" method="post">
    <label for="name">Name:</label>
    <input type="text" id="name" name="name">
    <br>
    <label for="cardNumber">VISA Card Number:</label>
    <input type="text" id="cardNumber" name="cardNumber">
    <br>
    <label for="address">Address:</label>
    <input type="text" id="address" name="address">
    <br>
    <div class="container">
        <button type="submit">Submit</button>
    </div>
</form>
</body>
</html>
