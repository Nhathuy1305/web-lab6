package com.main.workspace;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

@WebServlet(name = "shoppingServlet", value = "/ShoppingServlet")
public class ShoppingServlet extends HttpServlet {

    ArrayList<String> productsList = new ArrayList<>();
    ArrayList<String> quantitiesList = new ArrayList<>();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("name");
        if (username != null) {
            username = username.trim();
        }
        String cardNumber = req.getParameter("cardNumber");
        if (cardNumber != null) {
            cardNumber = cardNumber.trim();
        }
        String address = req.getParameter("address");
        if (address != null) {
            address = address.trim();
        }
        String[] products = req.getParameterValues("product");
        String[] quantities = req.getParameterValues("quantity");
        String deleteParam = req.getParameter("delete");

        Cookie[] cookies = req.getCookies();

        if (username != null) {
            Cookie nameCookie = new Cookie("username", URLEncoder.encode(username, StandardCharsets.UTF_8));
            nameCookie.setMaxAge(24 * 60 * 60); // 1 day
            resp.addCookie(nameCookie);
        }
        if (cardNumber != null) {
            Cookie cardNumberCookie = new Cookie("cardNumber", URLEncoder.encode(cardNumber, StandardCharsets.UTF_8));
            cardNumberCookie.setMaxAge(24 * 60 * 60); // 1 day
            resp.addCookie(cardNumberCookie);
        }
        if (address != null) {
            Cookie addressCookie = new Cookie("address", URLEncoder.encode(address, StandardCharsets.UTF_8));
            addressCookie.setMaxAge(24 * 60 * 60); // 1 day
            resp.addCookie(addressCookie);
        }

        if (deleteParam != null && deleteParam.equals("true")) {
            String productToRemove = req.getParameter("product");
            String quantityToRemove = req.getParameter("quantity");
            removeFromAccountBean(productToRemove, quantityToRemove);
            resp.setStatus(HttpServletResponse.SC_OK);
            return;
        } else {
            if (products != null && quantities != null && products.length == quantities.length) {
                for (int i = 0; i < products.length; i++) {
                    productsList.add(products[i]);
                    quantitiesList.add(quantities[i]);
                }
            }
        }

        String[] product = productsList.toArray(new String[0]);
        String[] quantity = quantitiesList.toArray(new String[0]);

        AccountBean accountBean = new AccountBean();
        accountBean.setName(username);
        accountBean.setCardNumber(cardNumber);
        accountBean.setAddress(address);
        accountBean.setProduct(product);
        accountBean.setQuantity(quantity);

        req.setAttribute("accountBean", accountBean);

        if (req.getParameter("checkout") != null) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("checkout.jsp");
            dispatcher.forward(req, resp);
        } else {
            RequestDispatcher dispatcher = req.getRequestDispatcher("products.jsp");
            dispatcher.forward(req, resp);
        }
    }

    private void removeFromAccountBean(String product, String quantity) {
        for (int i = 0; i < productsList.size(); i++) {
            if (productsList.get(i).equals(product) && quantitiesList.get(i).equals(quantity)) {
                productsList.remove(i);
                quantitiesList.remove(i);
                break;
            }
        }
    }

}
